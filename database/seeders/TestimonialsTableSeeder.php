<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Testimonials;
use Faker\Factory as Faker;

class TestimonialsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        Testimonials::create([
            'name' => 'John Doe',
            'message' => 'I was really impressed with the service I received. The team was professional and delivered on time.',
            'position' => $faker->jobTitle,
            'image' => 'testimonials-1.jpg',
            'posts_id' => 1,
        ]);

        Testimonials::create([
            'name' => 'Jane Smith',
            'message' => 'I had a great experience working with this company. They exceeded my expectations and I would recommend them to others.',
            'position' => $faker->jobTitle,
            'image' => 'testimonials-2.jpg',
            'posts_id' => 1,
        ]);

        Testimonials::create([
            'name' => 'Bob Johnson',
            'message' => 'The team was very responsive and delivered quality work. I would definitely work with them again.',
            'position' => $faker->jobTitle,
            'image' => 'testimonials-3.jpg',
            'posts_id' => 2,
        ]);
    }
}
