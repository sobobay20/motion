<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Media;

class MediaTableSeeder extends Seeder
{
    public function run()
    {

        Media::create([
            'posts_id' => 1,
            'media' => 'gallery-1.jpg',
            'type' => 'image',

        ]);


        Media::create([
            'posts_id' => 1,
            'media' => 'gallery-2.jpg',
            'type' => 'image',
        ]);

        Media::create([
            'posts_id' => 2,
            'media' => 'gallery-3.jpg',
            'type' => 'image',
        ]);



    }
}
