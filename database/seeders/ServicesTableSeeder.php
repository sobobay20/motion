<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Services;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            ['name' => 'Photography', 'description' => 'We offer professional photography services for events, portraits, and more.', 'price' => '100.00'],
            ['name' => 'Videography', 'description' => 'We offer professional videography services for events, commercials, and more.', 'price' => '200.00'],
            ['name' => 'Live Streaming', 'description' => 'We offer live streaming services for events, concerts, and more.', 'price' => '150.00'],
            ['name' => 'Promotional Ads', 'description' => 'We offer promotional ad services for businesses, products, and more.', 'price' => '250.00'],
            ['name' => 'Voice-over Recordings', 'description' => 'We offer voice-over recording services for commercials, videos, and more.', 'price' => '50.00'],
            ['name' => 'Motion Graphics', 'description' => 'We offer motion graphics services for videos, presentations, and more.', 'price' => '300.00'],
            ['name' => 'Graphic Design', 'description' => 'We offer graphic design services for logos, brochures, and more.', 'price' => '200.00'],
            ['name' => 'Animation', 'description' => 'We offer animation services for videos, presentations, and more.', 'price' => '350.00'],
            ['name' => 'Drone Services', 'description' => 'We offer drone services for aerial photography and videography.', 'price' => '400.00'],
            ['name' => 'Film Making', 'description' => 'We offer film making services for movies, documentaries, and more.', 'price' => '450.00'],
        ];

        foreach ($services as $service) {
            Services::create($service);
        }
    }

}
