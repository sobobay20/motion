<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Emmanuel Dankwah',
            'email' => 'motionplusstudios@gmail.com',
            'password' => bcrypt('password123'),
            'city' => 'Kumasi, Ghana',
            'about' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            'phone' => '+233240862910',
            'website' => 'www.motionplusstudios.com',
            'degree' => 'Masters',
            'freelance' => 'Available',
            'profile_picture' => 'testimonials-3.jpg'
        ]);
    }
}



