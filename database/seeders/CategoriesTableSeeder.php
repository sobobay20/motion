<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        Category::create([
            'name' => 'Photography',
            'desc' => 'Web design and development',
        ]);

        Category::create([
            'name' => 'Documentaries',
            'desc' => 'Graphic design and branding',
        ]);

        Category::create([
            'name' => 'Interviews',
            'desc' => 'Photography and videography',
        ]);

        Category::create([
            'name' => 'Trailers',
            'desc' => 'Photography and videography',
        ]);

        Category::create([
            'name' => 'Sports Videos',
            'desc' => 'Photography and videography',
        ]);

        Category::create([
            'name' => 'Travel Videos',
            'desc' => 'Photography and videography',
        ]);

        Category::create([
            'name' => 'Aerial Shots',
            'desc' => 'Photography and videography',
        ]);
    }
}
