<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Posts;

class PostsTableSeeder extends Seeder
{
    public function run()
    {

        Posts::create([
            'category_id' => 1,
            'title' => 'Web Design Project 1',
            'description' => 'A responsive web design project for a small business',
            'project_date' => '2022-01-01',
            'client_name' => 'Acme Inc.',
            'designer' => 'John Doe',
        ]);


        Posts::create([
            'category_id' => 2,
            'title' => 'Graphic Design Project 1',
            'description' => 'A branding project for a startup',
            'project_date' => '2022-02-01',
            'client_name' => 'Startup Inc.',
            'designer' => 'Jane Smith',
        ]);

        Posts::create([
            'category_id' => 3,
            'title' => 'Photography Project 1',
            'description' => 'A photography shoot for a fashion brand',
            'project_date' => '2022-03-01',
            'client_name' => 'Fashion Brand Inc.',
            'designer' => 'Robert Johnson',
        ]);



    }
}
