<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AddHeaders
{
    public function handle(Request $request, Closure $next)
    {
        if (!$this->shouldExcludeRoute($request)) {
            $response = $next($request);

            $response->header('X-Frame-Options', 'SAMEORIGIN');
            $response->header('Content-Security-Policy', "default-src 'self'");
        } else {
            $response = $next($request);
        }

        return $response;
    }

    protected function shouldExcludeRoute(Request $request)
    {
        // Modify the condition below to exclude the specific routes that are being affected
        if (Str::startsWith($request->route()->getName(), 'admin.')) {
            return true;
        }

        return false;
    }
}
