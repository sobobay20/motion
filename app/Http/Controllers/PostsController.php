<?php

namespace App\Http\Controllers;
use App\Models\Posts;
use App\Models\Media;
use App\Models\Category;
use App\Models\Testimonials;
use Illuminate\Http\Request;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostEditRequest;
use Illuminate\Support\Facades\Storage;


class PostsController extends Controller
{

    public function home()
    {
        $posts = Posts::with('media')->get();
        return view('admin.home', compact('posts'));
    }

    public function display()
    {
        $posts = Posts::with('media')->get();
        return view('posts.index', compact('posts'));
    }


    public function show($id)
    {
        $post = Posts::with('media','testimonials','category')->findOrFail($id);
        $categoryName= $post->category->name;
        return view('posts.show', compact('post', 'categoryName'));
    }

    public function index()
    {
        $posts = Posts::where('is_deleted', 0)->get();
        return view('admin.posts.index', ['posts' => $posts]);
    }


    public function create()
    {
        $categories = Category::All();
        return view('admin.posts.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'project_date' => 'required|date',
            'description' => 'required',
            'category_id' => 'required|numeric',
            'client_name' => 'required|max:255',
            'designer' => 'required|max:255',
        ]);

        $post = new Posts();
        $post->title = $validatedData['title'];
        $post->project_date = $validatedData['project_date'];
        $post->description = $validatedData['description'];
        $post->category_id = $validatedData['category_id'];
        $post->client_name = $validatedData['client_name'];
        $post->designer = $validatedData['designer'];

        if ($post->save()) {
            $postId = $post->id;

            if ($request->hasFile('media')) {
                $files = $request->all()['media'];

                foreach ($files as $file) {
                    $extension = $file->getClientOriginalExtension();
                    $type = '';
                    if ($extension === 'jpg' || $extension === 'jpeg' || $extension === 'png') {
                        $type = 'image';
                        $path = $file->move(public_path('assets/uploads/images'), $file->getClientOriginalName());
                    } else if ($extension === 'mp4' || $extension === 'avi') {
                        $type = 'video';
                        $path = $file->move(public_path('assets/uploads/videos'), $file->getClientOriginalName());
                    } else if ($extension === 'mp3' || $extension === 'wav') {
                        $type = 'audio';
                        $path = $file->move(public_path('assets/uploads/audios'), $file->getClientOriginalName());
                    }

                    $media = new Media();
                    $media->media = $file->getClientOriginalName();
                    $media->type = $type;
                    $media->posts_id = $postId;
                    if (!$media->save()) {
                        return back()->with('error', 'Media was not saved');
                    }
                }
            }
            return back()->with('success', 'Post created successfully.');
        } else {
            return back()->with('error', 'Post was not saved');
        }
    }


    public function update(PostEditRequest $request, $id)
    {
        $validatedData = $request->validated();
        $post = Posts::findOrFail($id);

        if ($request->hasFile('media')) {
            $files = $request->file('media');
            $existingFiles = Media::where('posts_id', $id)->get();

            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $type = '';
                if ($extension === 'jpg' || $extension === 'jpeg' || $extension === 'png') {
                    $type = 'image';
                    $path = $file->move(public_path('assets/uploads/images'), $file->getClientOriginalName());
                } else if ($extension === 'mp4' || $extension === 'avi') {
                    $type = 'video';
                    $path = $file->move(public_path('assets/uploads/videos'), $file->getClientOriginalName());
                } else if ($extension === 'mp3' || $extension === 'wav') {
                    $type = 'audio';
                    $path = $file->move(public_path('assets/uploads/audios'), $file->getClientOriginalName());
                }

                if ($existingFiles->count() > 0) {
                    $existingFiles->each(function ($existingFile) use ($post) {
                        $existingFile->delete();
                    });
                }

                $fileData = [
                    'posts_id' => $post->id,
                    'media' => $file->getClientOriginalName(),
                    'type' => $type,
                ];
                Media::create($fileData);
            }
        }

        if (!$post->update($validatedData)) {
            return back()->withErrors(['Error updating post']);
        }

        return back()->with('success', 'Post has been updated successfully!');
    }


    public function edit($id)
    {
        $post = Posts::find($id);
        $categories = Category::All();
        return view('admin.posts.edit', compact('post', 'categories'));
    }

    public function destroy($id)
    {
        $post = Posts::findOrFail($id);
        $post->is_deleted = 1;
        $post->save();
        return back()->with('success', 'Post deleted successfully');
    }
}

