<?php

namespace App\Http\Controllers;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $profile_picture_path = asset('assets/uploads/profile/'.$user->profile_picture);
        return view('admin.profile.edit', [
            'user' => $user,
            'profile_picture_path' => $profile_picture_path,
            'id' => $id
        ]);

    }


    public function update(Request $request, $id)
{
    $validatedData = $request->validate([
        'name' => 'required|max:255',
        'email' => 'required',
        'phone' => 'required|numeric',
    ]);

    $user = User::findOrFail($id);
    $user->name = $validatedData['name'];
    $user->email = $validatedData['email'];
    $user->phone = $validatedData['phone'];
    $user->website = $request->input('website');
    $user->city = $request->input('city');
    $user->degree = $request->input('degree');
    $user->freelance = $request->input('freelance');
    $user->about = $request->input('about');

    if ($request->hasFile('profile_picture')) {
        $profile_picture = $request->file('profile_picture');
        $filename = $profile_picture->getClientOriginalName();
        $profile_picture->move(public_path('assets/uploads/profile'), $filename);
        $user->profile_picture = $filename;
    }

    if ($user->save()) {
        return back()->with('success', 'User Profile updated successfully');
    } else {
        return back()->withErrors(['error', 'Unable to update user profile']);
    }
}




}
