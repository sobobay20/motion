<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Services;
use App\Models\Testimonials;
use App\Models\User;


class AboutController extends Controller
{

        public function index()
        {
            $user = User::find(1);
            $services = Services::all();
            $testimonials = Testimonials::all();
            return view('about', ['user' => $user,'services' => $services, 'testimonials' => $testimonials]);
        }



}

