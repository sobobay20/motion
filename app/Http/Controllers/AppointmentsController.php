<?php

namespace App\Http\Controllers;
// use Illuminate\Session\Middleware\StartSession;
use Illuminate\Http\Request;
// use App\Models\Testimonials;
use App\Models\Appointments;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;



class AppointmentsController extends Controller
{

    public function display($id)
{
    $appointment = Appointments::find($id);

    if ($appointment !== null) {
        return view('emails.contact', ['appointment' => $appointment]);
    } else {
        return redirect()->back()->with('error', 'Appointment not found!');
    }
}


public function contact()
{
    return view('contact');
}



    public function index()
    {
        $appointments = Appointments::all();
        if ($appointments !== null) {
            return view('admin.appointments.index', ['appointments' => $appointments]);
        } else {
            return redirect()->back()->with('error', 'Appointment not found!');
        }
    }


    public function store(Request $request)
    {
        $appointment = Appointments::create([
            'name' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'message' => $request->message,
            'attended_to' => false,
            'response_identifier' => uniqid()
        ]);

        Mail::send('emails.contact', ['appointment' => $appointment], function ($message) use ($appointment) {
            $message->from($appointment->email, $appointment->name);
            $message->to('motionplusstudios@gmail.com');
            $message->subject('Appointment/Enquiry Request Mail');
        });

        return redirect()->back()->with('success', 'Appointment Request sent successfully!');
    }




    public function update(Request $request, $id)
    {
        $appointment = Appointments::find($id);
        $attended_to = $request->input('attended_to') == '1' ? true : false;
        $appointment->update([
            'attended_to' => $attended_to,
            'response_timestamp' => now()
        ]);

        return redirect()->back()->with('success', 'Appointment updated.');
    }




public function destroy($id)
{
    $appointments = Appointments::findOrFail($id);
    $appointments->delete();
    return redirect()->back()->with('success', 'Appointment deleted Successfully.');
}
}




