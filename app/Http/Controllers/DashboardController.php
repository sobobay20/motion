<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posts;
use App\Models\Media;
use App\Models\User;
use App\Models\Appointments;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{



    public function index()
{
        if (Auth::check()) {
            $data = $this->getDataFromGoogleAnalyticsAPI();
            $appointments = Appointments::all();
            $recent_posts = Posts::with('media')->latest()->take(5)->get();
            $user = User::find(1);

            return view('admin.dashboard', compact('data', 'appointments', 'recent_posts', 'user'));
        } else {
            return redirect('login');
        }

}


    private function getDataFromGoogleAnalyticsAPI()
    {
        $data = "null";

        return $data;
    }



}
