<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use PragmaRX\Google2FA\Google2FA;

class LoginController extends Controller
{
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        // Save the intended URL to the session
        session(['url.intended' => url()->previous()]);

        return view('auth.login.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        $user = User::where('email', $credentials['email'])->first();

        // Check if the user exists and the password is correct
        if ($user && Hash::check($credentials['password'], $user->password)) {


            Auth::login($user, true);

            // Get the intended URL from the session if it exists, otherwise use the default dashboard route
            $intendedUrl = session('url.intended', route('admin.dashboard'));

            // If the intended URL is an admin URL and the user is not authorized, redirect to the login page
            if (strpos($intendedUrl, 'admin/') === 0 && !Auth::check()) {
                return redirect()->route('login')->withErrors([
                    'You must be logged in to access this page.',
                ]);
            }

            // Redirect to the intended URL
            return redirect($intendedUrl);
        }

        return redirect()->route('login')->withErrors([
            'email' => 'These credentials do not match our records.',
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
}
