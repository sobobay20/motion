<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            // User is authenticated, so we can show the home page
            return view('admin.dashboard');
        } else {
            // User is not authenticated, so redirect to the login page
            return redirect('login');
        }
    }
}
