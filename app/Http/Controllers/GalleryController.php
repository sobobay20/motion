<?php

namespace App\Http\Controllers;

use App\Models\Posts;
use App\Models\Category;
use App\Models\Media;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display a listing of the media (images or videos) based on category.
     *
     * @param int|null $categoryId
     * @return \Illuminate\Http\Response
     */


    public function index($category_id = null)
    {
        if ($category_id) {
            $posts = Posts::where('category_id', $category_id)->get();
            $category = Category::find($category_id);
            if($category->name){
             $category_name = $category->name;
             }
        } else {
            $posts = Posts::all();
            $category_name = 'All';
        }

        $categories = Category::all();
        $media = Media::whereIn('posts_id', $posts->pluck('id'))->get();

        return view('gallery.index', compact('posts', 'categories', 'category_name', 'media'));
    }





}
