<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'media' => 'required|file',
            'title' => 'required|string|max:255',
            'project_date' => 'required|date',
            'description' => 'required|string',
            'category_id' => 'required|numeric',
            'client_name' => 'required|string|max:255',
            'designer' => 'required|string|max:255',
        ];
    }

    public function messages()
    {
        return [
            'media.required' => 'Please select a photo or video',
            'title.required'  => 'Please enter a title',
            'project_date.required'  => 'Please select a date',
            'description.required'  => 'Please enter a description',
            'category_id.required'  => 'Please select a category',
            // 'project_url.required'  => 'Please enter a project url',
            'client_name.required'  => 'Please enter a client name',
            'designer.required'  => 'Please enter a designer',
        ];
    }
}
