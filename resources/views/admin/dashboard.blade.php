
@extends('layouts.dashboard')
@section('title', 'Dashboard')
@section('content')
<?php use Carbon\Carbon; ?>
<div class="container-scroller">

    <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
        <a class="sidebar-brand brand-logo" href="{{ route('admin.dashboard')}}"><img src="{{ asset('assets/uploads/logo/motion-plus-logo.png') }}" style="height: 100px; width: 100px;" alt="logo" /></a>
        <a class="sidebar-brand brand-logo-mini" href="{{ route('admin.dashboard')}}"><img src="{{ asset('assets/uploads/logo/motion-plus-logo.png') }}"  style="height: 50px; width: 50px; display:flex;" alt="logo" /></a>
      </div>
      <ul class="nav">
        <li class="nav-item profile">
          <div class="profile-desc">
            <div class="profile-pic">
              <div class="count-indicator">
                <img class="img-xs rounded-circle " src="{{ asset('assets/uploads/profile/'.$user->profile_picture) }}" alt="">
                <span class="count bg-success"></span>
              </div>
              <div class="profile-name">
                <h5 class="mb-0 font-weight-normal">{{ $user->name}}</h5>
                {{-- <span>Gold Member</span> --}}
              </div>
            </div>
            <a href="#" id="profile-dropdown" data-bs-toggle="dropdown"><i class="mdi mdi-dots-vertical"></i></a>
            <div class="dropdown-menu dropdown-menu-right sidebar-dropdown preview-list" aria-labelledby="profile-dropdown">

              <div class="dropdown-divider"></div>
              <a href="#recent-posts" class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-dark rounded-circle">
                    <i class="mdi mdi-onepassword  text-info"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <p class="preview-subject ellipsis mb-1 text-small">Recent Posts</p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#to-do-list" class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-dark rounded-circle">
                    <i class="mdi mdi-calendar-today text-success"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <p class="preview-subject ellipsis mb-1 text-small">To-do list</p>
                </div>
              </a>
            </div>
          </div>
        </li>
        <li class="nav-item nav-category">
          <span class="nav-link">Menu</span>
        </li>
        <li class="nav-item menu-items">
          <a class="nav-link" href="{{ route('admin.dashboard') }}">
            <span class="menu-icon">
              <i class="mdi mdi-speedometer"></i>
            </span>
            <span class="menu-title">Dashboard</span>
          </a>
        </li>

        <li class="nav-item menu-items">
          <a class="nav-link" href="{{ route('admin.posts.index') }}">
            <span class="menu-icon">
              <i class="mdi mdi-playlist-play"></i>
            </span>
            <span class="menu-title">Projects</span>
          </a>
        </li>
        <li class="nav-item menu-items">
          <a class="nav-link" href="{{ route('admin.testimonials.index') }}">
            <span class="menu-icon">
              <i class="mdi mdi-table-large"></i>
            </span>
            <span class="menu-title">Testimonials</span>
          </a>
        </li>
        <li class="nav-item menu-items">
          <a class="nav-link" href="{{ route('admin.services.index') }}">
            <span class="menu-icon">
              <i class="mdi mdi-chart-bar"></i>
            </span>
            <span class="menu-title">Services</span>
          </a>
        </li>
        <li class="nav-item menu-items">
          <a class="nav-link" href="{{ route('admin.categories.index') }}">
            <span class="menu-icon">
              <i class="mdi mdi-contacts"></i>
            </span>
            <span class="menu-title">Categories</span>
          </a>
        </li>

        <li class="nav-item menu-items">
          <a class="nav-link" href="{{ route('admin.appointments.index') }}">
            <span class="menu-icon">
              <i class="mdi mdi-file-document-box"></i>
            </span>
            <span class="menu-title">Appointments</span>
          </a>
        </li>


      </ul>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_navbar.html -->
      <nav class="navbar p-0 fixed-top d-flex flex-row">
        <div class="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
          <a class="navbar-brand brand-logo-mini" href="{{ route('admin.dashboard') }}"><img src="{{ asset('assets/uploads/logo/motion-plus-logo.png') }}" style="height: 40px; width: 150px;" alt="logo" /></a>
        </div>
        <div class="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
          </button>
          <ul class="navbar-nav w-100">
            <li class="nav-item w-100">
              <h4 class="text-center">Dashboard</h4>
            </li>
          </ul>
          <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item dropdown">
              <a class="nav-link" id="profileDropdown" href="#" data-bs-toggle="dropdown">
                <div class="navbar-profile">
                  <img class="img-xs rounded-circle" src="{{ asset('assets/uploads/profile/'.$user->profile_picture) }}" alt="">
                  <p class="mb-0 d-none d-sm-block navbar-profile-name">{{ $user->name}}</p>
                  <i class="mdi mdi-menu-down d-none d-sm-block"></i>
                </div>
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="profileDropdown">
                <h6 class="p-3 mb-0">Profile</h6>
                <div class="dropdown-divider"></div>
                <a href="{{ route('admin.profile.edit', '1') }}" class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-dark rounded-circle">
                      <i class="mdi mdi-settings text-success"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <p class="preview-subject mb-1">Settings</p>
                  </div>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();" class="dropdown-item preview-item">
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-dark rounded-circle">
                      <i class="mdi mdi-logout text-danger"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <p class="preview-subject mb-1">Log out</p>
                  </div>
                </a>

              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-format-line-spacing"></span>
          </button>
        </div>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 col-xl-12 col-12 grid-margin stretch-card">
                    <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Appointment Status</h4>
                        <div class=" table table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th> Client Name </th>
                                        <th> Client Email</th>
                                        <th> Subject </th>
                                        <th> Message </th>
                                        <th>Response Time</th>
                                        <th> Appointment Status </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($appointments as $appointment)
                                    <tr>
                                        <td>{{ $appointment->name }}</td>
                                        <td>{{ $appointment->email }}</td>
                                        <td>{{ $appointment->subject }}</td>
                                        <td>{{ $appointment->message }}</td>
                                        <td>
                                            @if($appointment->attended_to)
                                                @php
                                                    $createdAt = new Carbon($appointment->created_at);
                                                    $responseTimestamp = new Carbon($appointment->response_timestamp);
                                                    $diff = $createdAt->diff($responseTimestamp);
                                                    $responseTime = $diff->format('%H hours %i minutes');
                                                @endphp
                                                {{ $responseTime }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($appointment->attended_to)
                                            <div class="badge badge-outline-success">Attended To</div>
                                            @else
                                            <div class="badge badge-outline-danger">Not Attended To</div>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    </div>
                </div>

            </div>

          <div class="row">
            <div class="col-md-6 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Engagement & Bounce Rates</h4>
                        <canvas id="rates" class="rates"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-md-6 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Number of Views</h4>
                        <canvas id="views" class="views"></canvas>
                    </div>
                </div>
            </div>
          </div>

        <div class="row ">
            <div class="col-md-6 col-xl-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Outbound Clicks</h4>
                        <canvas id="clicks" class="clicks"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Video Engagements</h4>
                        <canvas id="video-engagement" class="clicks"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-md-6 col-xl-6 grid-margin stretch-card">
                <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Visitors by Countries</h4>
                    <div class="row">
                    <div class="col-md-7">
                        <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>
                                <i class="flag-icon flag-icon-us"></i>
                                </td>
                                <td>USA</td>
                                <td class="text-right"> 1500 </td>
                                <td class="text-right font-weight-medium"> 56.35% </td>
                            </tr>
                            <tr>
                                <td>
                                <i class="flag-icon flag-icon-de"></i>
                                </td>
                                <td>Germany</td>
                                <td class="text-right"> 800 </td>
                                <td class="text-right font-weight-medium"> 33.25% </td>
                            </tr>
                            <tr>
                                <td>
                                <i class="flag-icon flag-icon-au"></i>
                                </td>
                                <td>Australia</td>
                                <td class="text-right"> 760 </td>
                                <td class="text-right font-weight-medium"> 15.45% </td>
                            </tr>
                            <tr>
                                <td>
                                <i class="flag-icon flag-icon-gb"></i>
                                </td>
                                <td>United Kingdom</td>
                                <td class="text-right"> 450 </td>
                                <td class="text-right font-weight-medium"> 25.00% </td>
                            </tr>
                            <tr>
                                <td>
                                <i class="flag-icon flag-icon-ro"></i>
                                </td>
                                <td>Romania</td>
                                <td class="text-right"> 620 </td>
                                <td class="text-right font-weight-medium"> 10.25% </td>
                            </tr>
                            <tr>
                                <td>
                                <i class="flag-icon flag-icon-br"></i>
                                </td>
                                <td>Brasil</td>
                                <td class="text-right"> 230 </td>
                                <td class="text-right font-weight-medium"> 75.00% </td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div id="audience-map" class="vector-map"></div>
                    </div>
                    </div>
                </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Form Interactions</h4>
                        <canvas id="form-interaction" class="clicks"></canvas>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div id="recent-posts" class="col-md-6 col-xl-6 grid-margin stretch-card">
                <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Recent Posts</h4>
                    <div class="owl-carousel owl-theme full-width owl-carousel-dash portfolio-carousel" id="owl-carousel-basic">
                        @foreach($recent_posts as $post)
                        @foreach($post->media as $media)
                            @if($media->media != null)
                                @if($media->type == 'image')
                                <div class="item">
                                    <img src="{{ asset('assets/uploads/images/'.$media->media) }}" alt="">
                                </div>
                                @elseif($media->type == 'video')
                                <div class="item">
                                    <video src="{{ asset('assets/uploads/videos/'.$media->media) }}" class="img-fluid" alt="{{ $post->title }}" controls autoplay muted></video>
                                </div>
                                @endif
                            @endif
                        @endforeach
                        @endforeach
                    </div>

                </div>
                </div>
            </div>

            <div id="to-do-list" class="col-md-6 col-xl-6 grid-margin stretch-card">
                <div class="card">
                <div class="card-body">
                    <h4 class="card-title">To do list</h4>
                    <div class="add-items d-flex">
                    <input type="text" class="form-control todo-list-input" placeholder="enter task..">
                    <button class="add btn btn-primary todo-list-add-btn">Add</button>
                    </div>
                    <div class="list-wrapper">
                    <ul class="d-flex flex-column-reverse text-white todo-list todo-list-custom">
                        <li>
                        <div class="form-check form-check-primary">
                            <label class="form-check-label">
                            <input class="checkbox" type="checkbox"> Create invoice </label>
                        </div>
                        <i class="remove mdi mdi-close-box"></i>
                        </li>
                        <li>
                        <div class="form-check form-check-primary">
                            <label class="form-check-label">
                            <input class="checkbox" type="checkbox"> Meeting with Client </label>
                        </div>
                        <i class="remove mdi mdi-close-box"></i>
                        </li>
                        <li>
                        <div class="form-check form-check-primary">
                            <label class="form-check-label">
                            <input class="checkbox" type="checkbox"> Prepare for presentation </label>
                        </div>
                        <i class="remove mdi mdi-close-box"></i>
                        </li>
                        <li>
                        <div class="form-check form-check-primary">
                            <label class="form-check-label">
                            <input class="checkbox" type="checkbox"> Plan weekend outing </label>
                        </div>
                        <i class="remove mdi mdi-close-box"></i>
                        </li>
                        <li>
                        <div class="form-check form-check-primary">
                            <label class="form-check-label">
                            <input class="checkbox" type="checkbox"> Surprise my girlfriend </label>
                        </div>
                        <i class="remove mdi mdi-close-box"></i>
                        </li>
                    </ul>
                    </div>
                </div>
                </div>
            </div>
        </div>

        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>


@endsection
