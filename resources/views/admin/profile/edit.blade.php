<x-main>
    <main id="main" data-aos="fade" data-aos-delay="1500">

        <!-- ======= End Page Header ======= -->
        <div class="page-header d-flex align-items-center">
          <div class="container position-relative">
            <div class="row d-flex justify-content-center">
              <div class="col-lg-8 col-xl-8 col-md-12 col-sm-12 col-12 text-center">
                @if (session('success'))
                <div id="alert" class="alert alert-success">
                  {{ session('success') }}
                </div>
                @endif
                @if (session('status'))
                <div id="alert" class="alert alert-success">
                  {{ session('status') }}
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


                <h2>Profile Update</h2>

              </div>
            </div>
          </div>
        </div><!-- End Page Header -->

        <section class="sample-page">
          <div class="container" data-aos="fade-up">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                    <form method="POST" action="{{ route('admin.profile.update', ['id' => $user->id])}}" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                         <div class="row">
                            <div class="col-lg-3 col-xl-3 col-md-12 col-sm-12 col-12">
                                <div class="text-center">
                                    <div class="preview-container">
                                        <div id="preview-div-profile" class="img-thumbnail" style="background: url({{ $user->profile_picture ? $profile_picture_path : asset('assets/img/avatar.jpg') }}); background-repeat: no-repeat; background-size: cover; height: 200px; width: 100%; border: 3px #fff; border-radius: 5%; ">
                                        </div>
                                        <h6 class="mt-2 mb-2">Select Profile Image</h6>
                                        <input type="file" name="profile_picture" class="form-control" onchange="previewImage(this)">
                                    </div>
                                </div>
                              </div>
                              <div class="col-lg-9 col-xl-9 col-md-12 col-sm-12 col-12">
                                <div class="form-group mb-2">
                                    <label class="form-label" for="project-title">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $user->name }}"/>
                                </div>

                                <div class="form-group mb-2">
                                    <label class="form-label" for="project-title">Email</label>
                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}"/>
                                </div>

                                <div class="form-group mb-2">
                                    <label class="form-label" for="project-title">Phone</label>
                                    <input type="text" class="form-control" name="phone" value="{{ $user->phone }}"/>
                                </div>

                                <div class="form-group mb-2">
                                    <label class="form-label" for="project-title">Website</label>
                                    <input type="text" class="form-control" name="website" value="{{ $user->website }}"/>
                                </div>

                                <div class="form-group mb-2">
                                    <label class="form-label" for="project-title">City</label>
                                    <input type="text" class="form-control" name="city" value="{{ $user->city }}"/>
                                </div>

                                <div class="form-group mb-2">
                                    <label class="form-label" for="project-title">Degree</label>
                                    <input type="text" class="form-control" name="degree" value="{{ $user->degree }}"/>

                                </div>

                                <div class="form-group mb-2">
                                    <label class="form-label" for="customFile">About</label>
                                    <textarea class="form-control" rows="3" name="about">{{ $user->about }}</textarea>
                                </div>

                                <div class="form-group mb-2">
                                    <label class="form-label" for="customFile">Freelance</label>
                                    <select class="form-control" name="freelance">
                                        <option value="N/A" {{ $user->freelance == 'N/A' ? 'selected' : '' }} >Select Availability</option>
                                        <option value="Available" {{ $user->freelance == 'Available' ? 'selected' : '' }} >Available</option>
                                        <option value="Not Available" {{ $user->freelance == 'Not Available' ? 'selected' : '' }} >Not Available</option>
                                   </select>
                                </div>


                              </div>
                         </div>

                         <div class="row mt-2">
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary mb-2 hover:bg-indigo-700">Submit</button>
                            </div>
                         </div>

                    </form>
                </div>
            </div>

          </div>
        </section>


      </main><!-- End #main -->
</x-main>
