@extends('layouts.base')

@section('content')

  <main id="main" data-aos="fade" data-aos-delay="1500">

    <!-- ======= End Page Header ======= -->
    <div class="page-header d-flex align-items-center">
      <div class="container position-relative">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-6 text-center">
            <h2>Details</h2>
            
            @include('partials.quote')

          </div>
        </div>
      </div>
    </div><!-- End Page Header -->

    <!-- ======= Gallery Single Section ======= -->
    <section id="gallery-single" class="gallery-single">

      <div class="container">
        <div class="position-relative h-100">
            <div class="slides-1 portfolio-details-slider swiper">
              <div class="swiper-wrapper align-items-center">
                @foreach($post->media as $media)
                  @if($media->type === 'video')
                    <div class="swiper-slide">
                      <video src="{{ Storage::url('assets/uploads/videos/'.$media->media) }}" type="video/mp4" class="fixed-height" alt="{{ $post->title }}" controls>
                      </video>
                    </div>
                  @elseif($media->type === 'image')
                    <div class="swiper-slide">
                      <img src="{{ Storage::url('assets/uploads/images/'.$media->media) }}" class="fixed-height" alt="{{ $post->title }}">
                    </div>
                  @endif
                @endforeach
              </div>
              <div class="swiper-pagination"></div>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
          </div>

          <div class="row justify-content-between gy-4 mt-4">
            <div class="col-lg-8">
                <div class="portfolio-description mt-5">
                    <h2>{{ $post->title }}</h2>
                    <p class="mt-2">
                    {{ $post->description }}
                    </p>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="portfolio-info">
                    <h3>Project information</h3>
                    <ul>
                        <li><strong>Category</strong> <span>{{ $categoryName }}</span></li>
                        <li><strong>Client</strong> <span>{{ $post->client_name }}</span></li>
                        <li><strong>Project date</strong> <span>{{  date('jS M Y', strtotime( $post->project_date)) }}</span></li>
                        <li><strong>Designer</strong> <span>{{ $post->designer }}</span></li>
                        {{-- <li><strong>Project URL</strong> <a href="{{ $post->project_url }}">{{ $post->project_url }}</a></li> --}}
                        {{-- <li><a href="{{ $post->project_url }}" class="btn-visit align-self-start">Visit Website</a></li> --}}
                    </ul>
                </div>
            </div>

          </div>

          <div class="section-header">
            <h2>Testimonials</h2>
            <p>What the clients are saying about our service</p>
          </div>

          <div class="row gy-4">
            @foreach($post->testimonials as $testimonial)
            <div class="col-12 col-md-6 col-lg-6">
                {{-- <h2>Testimonials</h2> --}}
                <div class="testimonial-item mt-5">
                <p>
                    <i class="bi bi-quote quote-icon-left"></i>
                    {{ $testimonial->message }}
                    <i class="bi bi-quote quote-icon-right"></i>
                </p>
                <div class="mb-5">
                    @if (!empty($testimonial))
                            <img src="{{ Storage::url('public/assets/uploads/testimonials/'.$testimonial->image) }}" class="testimonial-img"  style="border-radius: 50%;" alt="">
                            @else
                                <img src="{{ asset('assets/img/avatar.jpg') }}" class="testimonial-img"  style="border-radius: 50%;" alt="">
                            @endif
                    <h3> {{ $testimonial->name }}</h3>

                    <h4 class="mt-2 mb-2">{{ $testimonial->position }}</h4>
                </div>
                </div>
            </div>
            @endforeach

          </div>
        </div>
      </div>
    </section><!-- End Gallery Single Section -->

  </main><!-- End #main -->

  @endsection
