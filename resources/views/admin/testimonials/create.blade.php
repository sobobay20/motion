<x-main>
    <main id="main" data-aos="fade" data-aos-delay="1500">

        <!-- ======= End Page Header ======= -->
        <div class="page-header d-flex align-items-center">
          <div class="container position-relative">
            <div class="row d-flex justify-content-center">
              <div class="col-lg-6 text-center">
                @if (session('success'))
                <div id="alert" class="alert alert-success">
                  {{ session('success') }}
                </div>
                @endif
                @if (session('status'))
                <div id="alert" class="alert alert-success">
                  {{ session('status') }}
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <h2>Testimonial Upload</h2>

              </div>
            </div>
          </div>
        </div><!-- End Page Header -->

        <section class="sample-page">
          <div class="container" data-aos="fade-up">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-xl-8 col-md-12 col-sm-12 col-12">
                    <form class="container-centered" method="POST" action=" {{ route('admin.testimonials.store')}}" enctype="multipart/form-data">
                        @csrf
                         <div class="row">
                            <div class="col-md-12 col-xl-3 col-lg-3 col-12 mt-2">
                                <div class="text-center">
                                    <div class="preview-container">
                                        <div id="preview-div" class="img-thumbnail">
                                        </div>
                                        <h6 class="mt-2 mb-2">Select testimonial Image</h6>
                                        <input type="file" name="image" class="form-control" onchange="previewImg(this)">
                                      </div>
                                  </div>

                              </div>

                              <div class="col-md-12 col-xl-9 col-lg-9 col-12 mt-2">
                                <div class="form-group mb-2">
                                    <label class="form-label" for="project-title">Name</label>
                                    <input type="text" class="form-control" name="name" />
                                </div>

                                <div class="form-group mb-2">
                                    <label class="form-label" for="customFile">Message</label>
                                    <textarea class="form-control" rows="3" name="message"></textarea>
                                </div>
                                <div class="form-group mb-2">
                                    <label class="form-label" for="customFile">Select Post for this Testimonials</label>
                                    <select class="form-control" name="posts_id">
                                       @foreach ($posts as $post)
                                           <option value="{{ $post->id }}" >{{ $post->title }}</option>
                                       @endforeach
                                   </select>
                                </div>

                                <div class="form-group mb-2">
                                   <label class="form-label" for="project-title">Position/Job Title</label>
                                   <input type="text" class="form-control" name="position" />
                               </div>
                              </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary mb-2 hover:bg-indigo-700">Submit</button>
                            </div>
                         </div>
                     </form>
                </div>
            </div>

          </div>
        </section>


      </main><!-- End #main -->
</x-main>
