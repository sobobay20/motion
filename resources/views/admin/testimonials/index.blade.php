<x-main>
    <main id="main" data-aos="fade" data-aos-delay="1500">

        <!-- ======= End Page Header ======= -->
        <div class="page-header d-flex align-items-center">
          <div class="container position-relative">
            <div class="row d-flex justify-content-center">
              <div class="col-lg-6 text-center">
                @if (session('success'))
                <div id="alert" class="alert alert-success">
                  {{ session('success') }}
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <h2>Testimonials</h2>
              </div>
            </div>
          </div>
        </div><!-- End Page Header -->

        <section class="sample-page">
          <div class="container" data-aos="fade-up">
            <div class="row mb-2">
                <div class="col-md-3 offset-md-9">
                    <a href="{{ route('admin.testimonials.create') }}">
                        <button type="button" class="btn btn-sm btn-primary float-right">
                           <i class="fa fa-plus text-white"></i> Add Testimonial
                         </button>
                    </a>
                </div>
            </div>
             <table class="table table-responsive-lg table-responsive-md table-responsive-sm">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Message</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($testimonials as $testimonial)
                    <tr>
                        <td>
                            @if (!empty($testimonial))
                            <img src="{{ asset('assets/uploads/testimonials/'.$testimonial->image) }}" class="img-ls"  style="border-radius: 50%;" alt="">
                            @else
                                <img src="{{ asset('assets/img/avatar.jpg') }}" class="img-ls"  style="border-radius: 50%;" alt="">
                            @endif

                        </td>
                        <td>{{ $testimonial->name }}</td>
                        <td>{{ $testimonial->message }}</td>
                        <td>
                            <a href="{{route('admin.testimonials.edit', $testimonial->id)}}">
                                <i class="fa fa-edit text-primary"></i>
                            </a>
                            <span>
                                <form action="{{ route('admin.testimonials.destroy', $testimonial->id) }}" method="POST" class="form-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" name="delete" onclick="
                                        event.preventDefault();
                                        Swal.fire({
                                            title: 'Are you sure?',
                                            text: 'You will not be able to recover this testimonial!',
                                            icon: 'warning',
                                            showCancelButton: true,
                                            confirmButtonText: 'Yes, delete it!',
                                            cancelButtonText: 'No, keep it'
                                        }).then((result) => {
                                            if (result.value) {
                                                this.closest('form').submit();
                                            }
                                        });
                                    ">
                                        <i class="fa fa-trash text-danger"></i>
                                    </button>
                                </form>

                            </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
             </table>
          </div>
        </section>


      </main><!-- End #main -->
</x-main>
