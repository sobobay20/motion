<?php use Carbon\Carbon; ?>

<x-main>
    <main id="main" data-aos="fade" data-aos-delay="1500">

        <!-- ======= End Page Header ======= -->
        <div class="page-header d-flex align-items-center">
          <div class="container position-relative">
            <div class="row d-flex justify-content-center">
              <div class="col-lg-6 text-center">
                @if (session('success'))
                <div id="alert" class="alert alert-success">
                  {{ session('success') }}
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <h2>All Appointments</h2>
              </div>
            </div>
          </div>
        </div><!-- End Page Header -->

        <section class="sample-page">
          <div class="container" data-aos="fade-up">

             <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Message</th>
                        <th>Attended To</th>
                        <th>Response Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($appointments as $appointment)
  <tr>
    <td>{{ $appointment->name }}</td>
    <td>{{ $appointment->email }}</td>
    <td style="width: 350px;">{{ $appointment->message }}</td>
    <td>
      @if ($appointment->attended_to)
        <span class="badge badge-success">Attended</span>
      @else
        <span class="badge badge-danger">Pending</span>
      @endif
    </td>
    <td>
        @if($appointment->attended_to)
            @php
                $createdAt = new Carbon($appointment->created_at);
                $responseTimestamp = new Carbon($appointment->response_timestamp);
                $diff = $createdAt->diff($responseTimestamp);
                $responseTime = $diff->format('%H hours %i minutes');
            @endphp
            {{ $responseTime }}
        @endif
    </td>

    <td style="width: 300px;">
        <div class="btn-group" role="group" aria-label="Action Buttons">
            <form action="{{ route('admin.appointments.update', $appointment->id) }}" method="post">
                @csrf
                @method('PUT')
                <input type="hidden" name="attended_to_state" value="{{ $appointment->attended_to }}">
                <input type="hidden" name="attended_to" value="{{ $appointment->attended_to ? '0' : '1' }}">
                <button type="submit" class="btn btn-outline-success">{{ $appointment->attended_to ? 'Unmark as Attended To' : 'Mark as Attended To' }}</button>
            </form>



           <form action="{{ route('admin.appointments.destroy',  $appointment->id) }}" method="POST">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-outline-danger" onclick="
          event.preventDefault();
          Swal.fire({
            title: 'Are you sure?',
            text: 'You will not be able to recover this appointment!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
          }).then((result) => {
            if (result.value) {
              this.closest('form').submit();
            }
          });
        ">
          Delete
        </button>
      </form>
          </div>
        {{--
            <button type="submit" class="btn btn-success">Mark as Attended</button>



      <form action="{{ route('admin.appointments.destroy',  $appointment->id) }}" method="POST">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-outline-danger" onclick="
          event.preventDefault();
          Swal.fire({
            title: 'Are you sure?',
            text: 'You will not be able to recover this appointment!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
          }).then((result) => {
            if (result.value) {
              this.closest('form').submit();
            }
          });
        ">
          Delete
        </button>
      </form> --}}
    </td>
  </tr>
@endforeach

                </tbody>
            </table>

          </div>
        </section>


      </main><!-- End #main -->
</x-main>
