<!-- ======= Footer ======= -->
<footer id="footer" class="footer mt-5">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>MotionPlus Studio</span></strong>. All Rights Reserved
      </div>
      <div class="credits" style="color: #000;">
        Designed by <a style="color: #000;" href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>


