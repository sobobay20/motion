<p class="mt-2">
    <q>The mysteries of life isn't a problem to solve, but ours is to effectively communicate your problems, ideas and vision through documentation.</q>
    <br>
    <span> - <i>Emmanuel Dankwah</i></span>
 </p>
 <a class="cta-btn mt-5" href="{{ route('contact')}}">Available for hire</a>
