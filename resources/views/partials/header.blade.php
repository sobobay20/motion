 <!-- ======= Header ======= -->
 <header id="header" class="header d-flex align-items-center fixed-top">
    <div class="container-fluid d-flex align-items-center justify-content-between">

      <a href="/" class="logo d-flex align-items-center  me-auto me-lg-0">
        <!-- Uncomment the line below if you also wish to use an image logo -->
         <img src="{{ asset('assets/uploads/logo/motion-plus-logo.png') }}" alt="">
        {{-- <i class="bi bi-camera"></i> --}}
        {{-- <h1>MotionPlus Studios</h1> --}}
      </a>

      <nav id="navbar" class="navbar">
        <ul>
                @if(Auth::check() && Auth::user())
                    <li><a href="{{ route('admin.dashboard') }}" class="active">Dashboard</a></li>
                    <li><a href="{{ route('admin.posts.index') }}">Projects</a></li>
                            <li><a href="{{ route('admin.testimonials.index') }}">Testimonials</a></li>
                            <li><a href="{{ route('admin.services.index') }}">Services</a></li>
                            <li><a href="{{ route('admin.categories.index') }}">Categories</a></li>
                            <li><a href="{{ route('admin.appointments.index') }}">Appointments</a></li>
                            <li><a href="{{ route('admin.profile.edit', '1') }}">Profile Settings</a></li>
                    <li>
                        <a href="#" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    @else
                    <li><a href="{{ route('posts.index') }}" class="active">Home</a></li>
                    <li><a href="{{ route('about') }}">About</a></li>
                    <li class="dropdown">
                        <a href="{{ route('gallery.index') }}">
                          <span>Gallery</span>
                          <i class="bi bi-chevron-down dropdown-indicator"></i>
                        </a>
                        <ul>
                            <li> <a href="{{ route('gallery.index') }}">
                                <span>All </span>
                              </a>
                            </li>
                          @foreach (\App\Models\Category::all() as $category)
                            <li><a href="{{ route('gallery.index', [ $category->id]) }}">{{ $category->name }}</a></li>
                          @endforeach
                        </ul>
                      </li>

                    <li><a href="{{ route('services.index') }}">Services</a></li>
                    <li><a href="{{ route('contact')}}">Contact</a></li>
            @endif
       </ul>


      </nav><!-- .navbar -->

      <div class="header-social-links">
        <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
        <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
        <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
      </div>
      <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
      <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

    </div>
  </header><!-- End Header -->
