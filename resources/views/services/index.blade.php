@extends('layouts.base')
@section('title', 'Services')
@section('content')

  <main id="main" data-aos="fade" data-aos-delay="1500">

    <!-- ======= End Page Header ======= -->
    <div class="page-header d-flex align-items-center">
      <div class="container position-relative">
        <div class="row d-flex justify-content-center mt-5">
          <div class="col-lg-12  col-12 col-md-12 col-sm-12 text-center">
            <h2>Services we provide</h2>
            @include('partials.quote')
          </div>
        </div>
      </div>
    </div><!-- End Page Header -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">
        <div class="row gy-4">
        @foreach($services as $service)
          <div class="col-lg-6 col-xl-3  col-12 col-sm-12 col-md-12">
            <div class="service-item position-relative">
              <i class="fa-solid fa-gear"></i>
              <h4><a href="" class="stretched-link">{{ $service->name }}</a></h4>
              <p>{{ $service->description }}</p>
            </div>
          </div><!-- End Service Item -->
          @endforeach
        </div>
      </div>
    </section><!-- End Services Section -->

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
      <div class="container">
        <div class="section-header">
          <h2>Prices</h2>
          <p>Check out our starting prices</p>
        </div>

        <div class="row gy-4 gx-lg-5">
            @foreach($services as $service)
          <div class="col-lg-12 col-xl-6 col-sm-12 col-md-12 col-12">
            <div class="pricing-item d-flex justify-content-between">
              <h3>{{ $service->name }}</h3>
              <h4>GHC {{ $service->price }}</h4>
            </div>
          </div><!-- End Pricing Item -->
          @endforeach
        </div>
    </section><!-- End Pricing Section -->

    <div style=" margin-top: 50px;"></div>
    <section id="testimonials" class="testimonials">
        <div class="container">

          <div class="section-header">
            <h2>Testimonials</h2>
            <p>What the clients are saying about our service</p>
          </div>

          <div class="slides-3 swiper">
            <div class="swiper-wrapper">
              @foreach($testimonials as $testimonial)
              <div class="swiper-slide">
                <div class="testimonial-item">
                  <div class="stars">
                    <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                  </div>
                  <p>
                      {{ $testimonial->message }}
                  </p>
                  <div class="profile mt-auto">
                    @if (!empty($testimonial))
                    <img src="{{ asset('assets/uploads/testimonials/'.$testimonial->image) }}" class="testimonial-img"  style="border-radius: 50%;" alt="">
                    @else
                        <img src="{{ asset('assets/img/avatar.jpg') }}" class="testimonial-img"  style="border-radius: 50%;" alt="">
                    @endif
                    <h3>{{ $testimonial->name }}</h3>
                    <h4>{{ $testimonial->position }}</h4>
                  </div>
                </div>
              </div><!-- End testimonial item -->
              @endforeach
            </div>
            <div class="swiper-pagination"></div>
          </div>

        </div>
    </section><!-- End Testimonials Section -->

  </main><!-- End #main -->

  @endsection
