@extends('layouts.base')
@section('title', 'About Us')
@section('content')

  <main id="main" data-aos="fade" data-aos-delay="1500">

    <!-- ======= End Page Header ======= -->
    <div class="page-header d-flex align-items-center">
      <div class="container position-relative">
        <div class="row d-flex justify-content-center mt-5">
            <div class="col-lg-12  col-12 col-md-12 col-sm-12 text-center">
            <h2>About</h2>
            @include('partials.quote')

          </div>
        </div>
      </div>
    </div><!-- End Page Header -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row gy-4 justify-content-center">
          <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
            @if (!empty($user->profile_picture))
            <img src="{{ asset('assets/uploads/profile/'.$user->profile_picture) }}" class="img-fluid" alt="">
            @else
                <img src="{{ asset('assets/img/avatar.jpg') }}" class="img-fluid"  alt="">
            @endif
          </div>
          <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 content">
            <h2>Professional Videographer from Accra, Ghana</h2>
            <p class="fst-italic py-3">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua.
            </p>
            <div class="row">
              <div class="col-xl-6 col-lg-12 col-12 col-sm-12 col-md-12">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>Website:</strong> <span>{{ $user->website }}</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Phone:</strong> <span>{{ $user->phone }}</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>City:</strong> <span>{{ $user->city }}</span></li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>Degree:</strong> <span>{{ $user->degree}}</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Email:</strong> <span>{{ $user->email}}</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Freelance:</strong> <span>{{ $user->freelance}}</span></li>
                </ul>
              </div>
            </div>
            <p class="py-3">
                {{ $user->about}}
            </p>

          </div>
        </div>

      </div>
    </section><!-- End About Section -->
 <!-- ======= Testimonials Section ======= -->
 <section id="testimonials" class="testimonials">
    <div class="container">

      <div class="section-header">
        <h2>Testimonials</h2>
        <p>What the clients are saying about our service</p>
      </div>

      <div class="slides-3 swiper">
        <div class="swiper-wrapper">
          @foreach($testimonials as $testimonial)
          <div class="swiper-slide">
            <div class="testimonial-item">
              <div class="stars">
                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
              </div>
              <p>
                  {{ $testimonial->message }}
              </p>
              <div class="profile mt-auto">
                @if (!empty($testimonial))
                <img src="{{ asset('assets/uploads/testimonials/'.$testimonial->image) }}" class="testimonial-img"  style="border-radius: 50%;" alt="">
                @else
                    <img src="{{ asset('assets/img/avatar.jpg') }}" class="testimonial-img"  style="border-radius: 50%;" alt="">
                @endif
                <h3>{{ $testimonial->name }}</h3>
                <h4>{{ $testimonial->position }}</h4>
              </div>
            </div>
          </div><!-- End testimonial item -->
          @endforeach
        </div>
        <div class="swiper-pagination"></div>
      </div>

    </div>
</section><!-- End Testimonials Section -->

  </main><!-- End #main -->
  @endsection
