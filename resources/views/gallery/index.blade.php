@extends('layouts.base')
@section('title', 'Gallery')
@section('content')

  <main id="main" data-aos="fade" data-aos-delay="1500">

  <!-- ======= End Page Header ======= -->
    <div class="page-header d-flex align-items-center">
        <div class="container position-relative">
           <div class="row d-flex justify-content-center mt-5">
              <div class="col-lg-6  col-12 col-md-12 col-sm-12 text-center">
                <h2>{{ $category_name }} </h2>
                @include('partials.quote')
              </div>
            </div>
        </div>
    </div><!-- End Page Header -->

   <!-- ======= Gallery Section ======= -->
   <section id="gallery" class="gallery">
    <div class="container-fluid">
      @if($posts)
      <div class="row gy-4 justify-content-center">
        @foreach($posts as $post)
          @foreach($media as $medium)
            @if($post->id == $medium->posts_id)
              @if($medium->type == 'image')
              <div class="col-xs-12 col-xl-3 col-lg-4 col-md-12 col-12">
                <div class="gallery-item h-100">
                    <img src="{{ asset('assets/uploads/images/'.$medium->media) }}" class="img-fluid" alt="{{ $post->title }}">

                    {{-- <img src="{{ Storage::url('assets/uploads/images'.$medium->media) }}" class="img-fluid" alt="{{ $post->title }}"> --}}
                    <div class="gallery-links d-flex align-items-center justify-content-center">
                      <a href="{{ asset('assets/uploads/images/'.$medium->media) }}" title="{{ $post->title }}" class="glightbox preview-link"><i class="bi bi-arrows-angle-expand"></i></a>
                      <a href="{{ route('posts.show', $post->id) }}" title="View Details" class="details-link"><i class="bi bi-link-45deg"></i></a>
                    </div>
                  </div>
              </div>
              @elseif($medium->type == 'video')
              <div class="col-xs-12 col-xl-3 col-lg-4 col-md-12 col-12">
                <div class="gallery-item">
                  <video src="{{ asset('assets/uploads/videos/'.$medium->media) }}" class="img-fluid" alt="{{ $post->title }}" controls autoplay muted></video>
                  <div class="gallery-links d-flex align-items-center justify-content-center">
                    <a href="{{ route('posts.show', $post->id) }}" title="View Details" class="details-link"><i class="bi bi-link-45deg"></i></a>
                  </div>
                </div>
              </div>
              @endif
            @endif
          @endforeach
        @endforeach
      </div>
      @endif
    </div>
  </section><!-- End Gallery Section -->

  </main><!-- End #main -->

  @endsection
