<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>@yield('title') - MotionPlus Studios</title>
    {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
    <!-- Favicons -->
  <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Inter:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Cardo:ital,wght@0,400;0,700;1,400&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/glightbox/css/glightbox.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/fontawesome/css/fontawesome.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/fontawesome/css/brands.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/fontawesome/css/solid.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/sweetalert/sweetalert.min.css') }}" rel="stylesheet">
  <script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
  <!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-PB3G5N1D3C"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-PB3G5N1D3C');
</script>

</head>
<body>
    @include('partials.header')
    @yield('content')
    @include('partials.footer')
    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <div id="preloader">
      <div class="line"></div>
    </div>

     <!-- Vendor JS Files -->
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>
  <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/main.js') }}"></script>
  <script>
    setTimeout(function(){
        document.getElementById("alert").style.display = "none";
    }, 10000);
    </script>



<script>
  function previewImage(input) {
    let previewDiv = document.querySelector("#preview-div-profile");

    if (input.files && input.files[0]) {
      let reader = new FileReader();
      reader.onload = function(e) {
        let imageUrl = e.target.result;
        previewDiv.style.backgroundImage = `url(${imageUrl})`;
      }
      reader.readAsDataURL(input.files[0]);
    } else {
      previewDiv.style.backgroundImage = `url({{ asset('assets/img/avatar.jpg') }})`;
    }
  }
</script>

<script>
    function previewImg(input) {
      let previewDiv = document.querySelector("#preview-div");

      if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function(e) {
          let imageUrl = e.target.result;
          previewDiv.style.backgroundImage = `url(${imageUrl})`;
        }
        reader.readAsDataURL(input.files[0]);
      } else {
        previewDiv.style.backgroundImage = `url({{ asset('assets/img/avatar.jpg') }})`;
      }
    }
  </script>




</body>
</html>

