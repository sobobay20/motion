<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>@yield('title') - MotionPlus Studios</title>
    <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('dash/assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dash/assets/vendors/css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('dash/assets/vendors/jvectormap/jquery-jvectormap.css') }}">
    <link rel="stylesheet" href="{{ asset('dash/assets/vendors/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dash/assets/vendors/owl-carousel-2/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dash/assets/vendors/owl-carousel-2/owl.theme.default.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{ asset('dash/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('dash/assets/css/mystyle.css') }}">

    <style>
        .nav-item.active .nav-link {
            color: rgb(44, 44, 46);
            }

        canvas {
                -moz-user-select: none;
                -webkit-user-select: none;
                -ms-user-select: none;
                }
      </style>
    <!-- End layout styles -->


  </head>
  <body>
    @yield('content')

    @include('partials.footer')

     <div id="preloader" >
        <div class="line"></div>
    </div>


    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset('dash/assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('dash/assets/vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('dash/assets/vendors/progressbar.js/progressbar.min.js') }}"></script>
    <script src="{{ asset('dash/assets/vendors/jvectormap/jquery-jvectormap.min.js') }}"></script>
    <script src="{{ asset('dash/assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('dash/assets/vendors/owl-carousel-2/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('dash/assets/js/jquery.cookie.js') }}" type="text/javascript"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('dash/assets/js/off-canvas.js') }}"></script>
    <script src="{{ asset('dash/assets/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('dash/assets/js/misc.js') }}"></script>
    <script src="{{ asset('dash/assets/js/settings.js') }}"></script>
    <script src="{{ asset('dash/assets/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{ asset('dash/assets/js/dashboard.js') }}"></script>
    <!-- End custom js for this page -->

    <script>
        var ctx = document.getElementById('rates').getContext('2d');
         var chart = new Chart(ctx, {
             type: 'bar',
             data: {
                 labels: ['Home Page', 'About Page', 'Services Page', 'Gallery Page', 'Details Page', 'Contact Page'],
                 datasets: [
                     {
                         label: 'Engagement Rate',
                         data: [65, 59, 80, 81, 56, 55],
                         backgroundColor: 'rgba(26, 127, 100, 0.8)',
                     },
                     {
                         label: 'Bounce Rate',
                         data: [40, 50, 30, 20, 10, 15],
                         backgroundColor: 'rgba(75, 192, 192, 0.2)',
                     }
                 ]
             },
             options: {
                 scales: {
                     xAxes: [{
                         barThickness: 40,
                         categorySpacing: 10
                     }]
                 }
             }
         });

    </script>

    {{-- <script>
      var ctx = document.getElementById('views').getContext('2d');
        var chart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Home Page', 'About Page', 'Services Page', 'Gallery Page', 'Details Page', 'Contact Page'],
            datasets: [
            {
                label: 'Number of Views',
                data: [200, 150, 300, 100, 400, 250],
                borderColor: 'rgba(63, 123, 227, 1)',
                backgroundColor: 'rgba(63, 123, 227, 0.2)',
                fill: false
            }
            ]
        },
        options: {
            scales: {
            yAxes: [{
                ticks: {
                beginAtZero: true
                }
            }]
            }
        }
        });

    </script> --}}



    <script>
        var ctx = document.getElementById('views').getContext('2d');
        var chart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [
              {
                label: 'Home Page',
                data: [100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650],
                borderColor: 'rgba(63, 123, 227, 1)',
                backgroundColor: 'rgba(63, 123, 227, 0.2)',
                fill: false
              },
              {
                label: 'About Page',
                data: [200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750],
                borderColor: 'rgba(255, 99, 132, 1)',
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                fill: false
              },
              {
                label: 'Services Page',
                data: [150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700],
                borderColor: 'rgba(255, 159, 64, 1)',
                backgroundColor: 'rgba(255, 159, 64, 0.2)',
                fill: false
              },
              {
                label: 'Gallery Page',
                data: [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600],
                borderColor: 'rgba(54, 162, 235, 1)',
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                fill: false
              },
              {
                label: 'Details Page',
                data: [300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850],
                borderColor: 'rgba(153, 102, 255, 1)',
                backgroundColor: 'rgba(153, 102, 255, 0.2)',
                fill: false
              },
              {
                label: 'Contact Page',
                data: [250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800],
                borderColor: 'rgba(75, 192, 192, 1)',
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                fill: false
              }
            ]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });
    </script>

    <script>
        var ctx = document.getElementById('clicks').getContext('2d');
        var chart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [
            {
                label: 'Home Page',
                data: [100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650],
                borderColor: 'rgba(63, 123, 227, 1)',
                backgroundColor: 'rgba(63, 123, 227, 0.2)',
                fill: false
            },
            {
                label: 'About Page',
                data: [200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750],
                borderColor: 'rgba(255, 99, 132, 1)',
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                fill: false
            },
            {
                label: 'Services Page',
                data: [150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700],
                borderColor: 'rgba(255, 159, 64, 1)',
                backgroundColor: 'rgba(255, 159, 64, 0.2)',
                fill: false
            },
            {
                label: 'Gallery Page',
                data: [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600],
                borderColor: 'rgba(54, 162, 235, 1)',
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                fill: false
            },
            {
                label: 'Details Page',
                data: [300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850],
                borderColor: 'rgba(153, 102, 255, 1)',
                backgroundColor: 'rgba(153, 102, 255, 0.2)',
                fill: false
            },
            {
                label: 'Contact Page',
                data: [250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800],
                borderColor: 'rgba(75, 192, 192, 1)',
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                fill: false
            }
            ]
        },
        options: {
            scales: {
            yAxes: [{
                ticks: {
                beginAtZero: true
                }
            }]
            }
        }
        });
    </script>


    <script>
        var ctx = document.getElementById('video-engagement').getContext('2d');
        var chart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [
            {
                label: 'Home Page',
                data: [100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650],
                borderColor: 'rgba(63, 123, 227, 1)',
                backgroundColor: 'rgba(63, 123, 227, 0.2)',
                fill: false
            },


            {
                label: 'Gallery Page',
                data: [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600],
                borderColor: 'rgba(54, 162, 235, 1)',
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                fill: false
            },
            {
                label: 'Details Page',
                data: [300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850],
                borderColor: 'rgba(153, 102, 255, 1)',
                backgroundColor: 'rgba(153, 102, 255, 0.2)',
                fill: false
            }

            ]
        },
        options: {
            scales: {
            yAxes: [{
                ticks: {
                beginAtZero: true
                }
            }]
            }
        }
        });
    </script>

    <script>
        var ctx = document.getElementById('form-interaction').getContext('2d');
        var formSubmissionsChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                datasets: [{
                label: 'Form Interactions',
                data: [12, 19, 3, 5, 2, 3, 8, 9, 5, 2, 3, 7],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
                }]
            },
            options: {
                scales: {
                yAxes: [{
                    ticks: {
                    beginAtZero: true
                    }
                }]
                }
            }
            });

    </script>

    <script>
      $(document).ready(function() {
        $('.nav-item').removeClass('active');
        });
    </script>

  </body>
</html>
