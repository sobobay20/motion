@extends('layouts.base')
@section('title', 'Home')
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="hero" class="hero d-flex flex-column justify-content-center align-items-center" data-aos="fade" data-aos-delay="1500">
    <div class="container">
      <div class="row justify-content-center mt-2">
        <div class="col-lg-6  col-12 col-md-12 col-sm-12 text-center">
            <p class="mt-2">
                <q>The mysteries of life isn't a problem to solve, but ours is to effectively communicate your problems, ideas and vision through documentation.</q>
                <br>
                <span> - <i>Emmanuel Dankwah</i></span>
             </p>
          <a href="{{ route('contact')}}" class="btn-get-started mt-2">Available for hire</a>
        </div>
      </div>
    </div>
  </section><!-- End Hero Section -->

  <main id="main" data-aos="fade" data-aos-delay="1500">

    <!-- ======= Gallery Section ======= -->
    <section id="gallery" class="gallery">
      <div class="container-fluid">
        <div class="row gy-4 justify-content-center">
            @foreach($posts as $post)
                @foreach($post->media as $media)
                    @if($media->media != null)
                        @if($media->type == 'image')
                            <div class="col-xs-12 col-xl-3 col-lg-4 col-md-12 col-12">
                                <div class="gallery-item h-100">
                                    {{-- <a href="{{ route('admin.show', $post->id) }}"> --}}
                                        <img src="{{ asset('assets/uploads/images/'.$media->media) }}" class="img-fluid" alt="{{ $post->title }}">
                                        <div class="gallery-links d-flex align-items-center justify-content-center">
                                            <a href="{{ asset('assets/uploads/images/'.$media->media) }}" title="{{ $post->title }}" class="glightbox preview-link"><i class="bi bi-arrows-angle-expand"></i></a>
                                            <a href="{{ route('posts.show',  $post->id) }}" title="View Details" class="details-link"><i class="bi bi-link-45deg"></i></a>
                                        </div>
                                </div>
                            </div>
                        @elseif($media->type == 'video')
                            <div class="col-xs-12 col-xl-3 col-lg-4 col-md-12">
                                <div class="gallery-item">
                                    {{-- <a href="{{ route('admin.show', $post->id) }}"> --}}
                                        <video src="{{ asset('assets/uploads/videos/'.$media->media) }}" class="img-fluid" alt="{{ $post->title }}" controls autoplay muted loop></video>
                                        <div class="gallery-links d-flex align-items-center justify-content-center">
                                            <a href="{{ route('posts.show',  $post->id) }}" title="View Details" class="details-link"><i class="bi bi-link-45deg"></i></a>
                                        </div>
                                    {{-- </a> --}}
                                </div>
                            </div>
                        @endif
                    @endif
                @endforeach
            @endforeach
        </div>


      </div>
    </section><!-- End Gallery Section -->
  </main><!-- End #main -->
@endsection


