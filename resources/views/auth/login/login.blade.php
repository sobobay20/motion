<x-login>
    <main id="main" data-aos="fade" data-aos-delay="1500">

        <!-- ======= End Page Header ======= -->
        <div class="page-header d-flex align-items-center">
          <div class="container position-relative">
            <div class="row d-flex justify-content-center">
              <div class="col-lg-6 text-center">
                @if (session('status'))
                <div id="alert" class="alert alert-success">
                  {{ session('status') }}
                </div>
                @endif

                <a href="{{ route('posts.index')}}">
                    <span class="justify-content-center">
                        <h2 class="font-size-lg">
                            <i class="bi bi-camera"></i>
                             MotionPlus Studios
                            </h2>
                      </span>
                </a>

                <h3 class="mt-5">Login</h3>

                <p class="mt-2">Login to enjoy admin privileges</p>

        <div class="mt-2">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        </div>
              </div>
            </div>
          </div>
        </div><!-- End Page Header -->


        <section class="sample-page">
          <div class="container" data-aos="fade-up">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <form class="container-centered" method="POST" action="{{ route('login') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group mb-2">
                            <label class="form-label" for="project-title">Email</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group mb-2">
                            <label class="form-label" for="project-title">Password</label>
                            <input type="password" class="form-control"  @error('password') is-invalid @enderror name="password" required autocomplete="current-password"/>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        </div>


                        <div class="row mb-0 mt-2 mb-2">
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary mt-2">
                                    Login
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

          </div>
        </section>


      </main><!-- End #main -->
</x-login>
