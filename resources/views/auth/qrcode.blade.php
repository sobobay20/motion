
<x-login>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">{{ __('Scan this QR code with the Google Authenticator app') }}</div>
                    <div class="card-body text-center">
                        <img src="{{ $qrCodeDataUrl }}" alt="QR Code">
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-login>
