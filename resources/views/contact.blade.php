
@extends('layouts.base')
@section('title', 'Contact Us')
@section('content')

  <main id="main" data-aos="fade" data-aos-delay="1500">

    <!-- ======= End Page Header ======= -->
    <div class="page-header d-flex align-items-center">
      <div class="container position-relative">
        <div class="row d-flex justify-content-center mt-5">
          <div class="col-lg-12  col-12 col-md-12 col-sm-12 text-center">
            <h2>Contact</h2>
            <p class="mt-2">
                <q>The mysteries of life isn't a problem to solve, but ours is to effectively communicate your problems, ideas and vision through documentation.</q>
                <br>
                <span> - <i>Emmanuel Dankwah</i></span>
             </p>
          </div>
        </div>
      </div>
    </div><!-- End Page Header -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">
        <div class="section-header text-center">
            <h2>Appointments</h2>
            <p>Book an appointment or make enquiries</p>
            <span class="mt-5">
                To make inquiries or book our services for video and photo content production and event coverage, you can reach us through our email by filling out your information in the form provided, or by calling us directly.
             </span>
          </div>

        <div class="row gy-4 justify-content-center mt-5">

          <div class="col-lg-3 col-xl-3 col-sm-12 col-md-12">
            <div class="info-item d-flex">
              <i class="bi bi-geo-alt flex-shrink-0"></i>
              <div>
                <h4>Location:</h4>
                <p>KNUST, Kumasi.</p>
              </div>
            </div>
          </div><!-- End Info Item -->

          <div class="col-lg-3 col-xl-3 col-sm-12 col-md-12">
            <div class="info-item d-flex">
              <i class="bi bi-envelope flex-shrink-0"></i>
              <div>
                <h4>Email:</h4>
                <p>motionplusstudios@gmail.com</p>
              </div>
            </div>
          </div><!-- End Info Item -->

          <div class="col-lg-3">
            <div class="info-item d-flex">
              <i class="bi bi-phone flex-shrink-0"></i>
              <div>
                <h4>Call:</h4>
                <p>+233 240 862 910</p>
              </div>
            </div>
          </div><!-- End Info Item -->

        </div>



        <div class="row justify-content-center mt-4">

          <div class="col-lg-8 col-xl-8 col-12 col-sm-12 col-md-12">
            <form action="{{ route('contact.store') }}" method="post">
                @csrf
                <div class="row">
                  <div class="col-12 form-group">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                  </div>
                  <div class="col-12 form-group mt-3">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                  </div>
                </div>
                <div class="form-group mt-3">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                </div>
                <div class="form-group mt-3">
                  <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                </div>

                <div class="mt-5"><button type="submit" class="btn btn-primary">Send Message</button></div>
              </form>
          </div><!-- End Contact Form -->

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  @endsection
